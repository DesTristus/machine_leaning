import os
import json
from time import time
from glob import glob

import numpy as np
import matplotlib
matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from skimage import io, transform, img_as_float
from tqdm import tqdm

from keras.models import Input, Model, load_model
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D, Dropout, concatenate
from keras.optimizers import Adam
from keras import backend as K
import argparse

import tensorflow as tf

import jpype
import getpass
import shutil


parser = argparse.ArgumentParser()
parser.add_argument("--model","--m", required=True,help="Path to model to use")
parser.add_argument("--inputPath","--i","--in", required=True ,help="Path to model to use")
parser.add_argument("--outputPath","--o","--out", required=True ,help="Path to model to use")
args = parser.parse_args()


# Loss function : the Jaccard index
def jaccard_coef(y_true, y_pred):
    """
    The Jaccard index, where 1 is the perfect overlap.
    """
    y_true_flat = K.flatten(y_true)
    y_pred_flat = K.flatten(y_pred)
    intersection = K.sum(y_true_flat * y_pred_flat)
    union = K.sum(y_true_flat) + K.sum(y_pred_flat) - intersection
    return intersection / (union + 1e-6)

def jaccard_coef_loss(y_true, y_pred):
    """
    Jaccard loss -- negative of the Jaccard coefficient, as losses are minimised.
    """
    return -jaccard_coef(y_true, y_pred)
    
    
original_filenames = sorted(glob(""+args.inputPath+"/*.tif"))
print("J'ai trouve {} fichiers.".format(len(original_filenames)))


model = load_model(
    args.model, 
    custom_objects={"jaccard_coef_loss": jaccard_coef_loss, "jaccard_coef": jaccard_coef}
)
if jpype.isJVMStarted() :
    print("JVM already started")
else :
    jpype.startJVM(jpype.getDefaultJVMPath(), classpath=['omero-5.6.0-jar-with-dependencies.jar'], convertStrings=True)

original_images = []
labelled_images = []
image_names=[]

# Iteration sur les fichiers
for filename in tqdm(original_filenames):
    
    # Lecture de l'image propre et separation des epaisseurs
    image = io.imread(filename)
    for z_slice in range(image.shape[0]):
        original_images.append(img_as_float(image[z_slice, :,:]))
        image_names.append(filename+"_"+str(z_slice))
    for z_slice in range(image.shape[0]):
            labelled_images.append(img_as_float(image[z_slice,:, :]))
        
print("J'ai {} epaisseurs depuis nos {} fichiers.".format(len(original_images), len(original_filenames)))

# Trouvons la taille de chaque images en (x, y)
image_sizes = np.array([image.shape for image in original_images])
assert np.all(np.array([image.shape for image in labelled_images]) == image_sizes)

# Le max, et la puissance de 2 immediatement au dessus de ce max
max_sizes = image_sizes.max(axis=0)
transform_to = 2 ** np.ceil(np.log2(max_sizes)).astype(int)
print("Les dimensions max sont {}.".format(max_sizes))
print("On va donc passer a la prochaine puissance de 2, qui est {}.".format(transform_to))


X = []
y = []
for image in tqdm(original_images, desc="Images propres"):
    X.append(np.pad(image, ((0, transform_to[0] - image.shape[0]), (0, transform_to[1] - image.shape[1])), mode="symmetric"))
for image in tqdm(labelled_images, desc="Images annotees"):
    y.append(np.pad(image, ((0, transform_to[0] - image.shape[0]), (0, transform_to[1] - image.shape[1])), mode="symmetric"))        



X = np.array(X)
X = X.reshape(*X.shape, 1)
y = np.array(y)
y = y.reshape(*y.shape, 1)

# Validation que toutes nos images ont la forme qu'il faut
assert np.all(X.shape[1:]  == (*transform_to, 1))


prediction_idx1 = np.arange(0,len(image_names))
test_sizes1 = image_sizes[prediction_idx1]
X_test1 = X
y_test1 = X

random_original_images1 = X_test1[prediction_idx1]
random_labelled_images1 = y_test1[prediction_idx1]
random_image_sizes1 = test_sizes1[prediction_idx1]

predictions1 = model.predict(random_original_images1)


n_images1=len(image_names)
# Coupons les images a la taille d'origine
rescaled_original1, rescaled_labelled1, rescaled_predictions1 = [], [], []
for i in range(n_images1):
    rescaled_original1.append(random_original_images1[i][:random_image_sizes1[i][0], :random_image_sizes1[i][1], 0])
    rescaled_labelled1.append(random_labelled_images1[i][:random_image_sizes1[i][0], :random_image_sizes1[i][1], 0])
    rescaled_predictions1.append(predictions1[i][:random_image_sizes1[i][0], :random_image_sizes1[i][1], 0])
    rescaled_predictions1[i] = np.array(rescaled_predictions1[i]).astype(np.float32)
    rescaled_predictions1[i] = (255 * rescaled_predictions1[i]).astype(np.uint8)
    
image_plus=[]
IJ = jpype.JPackage("ij").IJ

j=0
for i in (original_filenames):
    size_x = IJ.openImage(i).getWidth();
    size_y = IJ.openImage(i).getHeight();
    size_z = IJ.openImage(i).getNSlices();

    imp = IJ.createHyperStack("tmp", size_x, size_y, 1, size_z, 1, 8);

    cal = IJ.openImage(i).getCalibration();
    
   
    imp.setCalibration(cal);

    stack = imp.getImageStack();
    
    j_temp = j
    for j in range(j_temp, j_temp + size_z):
        values = rescaled_predictions1[j].flatten()
        values = jpype.JArray(jpype.JByte)(values)

        n = imp.getStackIndex(1, j - j_temp +1, 1);
        stack.setPixels(values, n)
        j+=1

    imp.setStack(stack);
    imp.setOpenAsHyperStack(True);
    imp.setDisplayMode(IJ.COMPOSITE);
    
    image_plus.append(imp)
    

IJIO = jpype.JPackage("ij.io")

path = "TEST_APPLY_MODEL/PREDICTION/"
#os.mkdir(path)

for i in range(len(image_plus)):
    file_saver = IJIO.FileSaver(image_plus[i])
    file_saver.saveAsTiff(args.outputPath+"/" + str(original_filenames[i].split('/')[-1]).replace('.tif', '')+"_MLprediction.tif")
