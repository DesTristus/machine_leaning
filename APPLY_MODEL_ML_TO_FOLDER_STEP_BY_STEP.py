import os
import json
from time import time
from glob import glob

import numpy as np
import matplotlib
import matplotlib.image as mpimg
matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from skimage import io, transform, img_as_float
from tqdm import tqdm
from tensorflow.keras.models import load_model
#from keras.models import Input, Model, load_model
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D, Dropout, concatenate
from keras.optimizers import Adam
from keras import backend as K
import argparse

import tensorflow as tf

import jpype
import getpass
import shutil


parser = argparse.ArgumentParser()
parser.add_argument("--model","--m", required=True,help="Path to model to use")
parser.add_argument("--inputPath","--i","--in", required=True ,help="Path to model to use")
parser.add_argument("--outputPath","--o","--out", required=True ,help="Path to model to use")
args = parser.parse_args()


# Loss function : the Jaccard index
def jaccard_coef(y_true, y_pred):
    """
    The Jaccard index, where 1 is the perfect overlap.
    """
    y_true_flat = K.flatten(y_true)
    y_pred_flat = K.flatten(y_pred)
    intersection = K.sum(y_true_flat * y_pred_flat)
    union = K.sum(y_true_flat) + K.sum(y_pred_flat) - intersection
    return intersection / (union + 1e-6)


def jaccard_coef_loss(y_true, y_pred):
    """
    Jaccard loss -- negative of the Jaccard coefficient, as losses are minimised.
    """
    return -jaccard_coef(y_true, y_pred)

#original_filenames = sorted(glob("/home/tridubos/MACHINE_LEARNING/IMAGE_TEST/RAW/*.tif"))
original_filenames = sorted(glob("" + args.inputPath + "/*.tif"))
print("J'ai trouve {} fichiers.".format(len(original_filenames)))

model = load_model(
     args.model,
    #"/home/tridubos/MACHINE_LEARNING/RECUPE_BOWSER/TEST_SCRIPT_BOWSER_MAI-2020/unet_profond_COL0_129_OLD.h5",
    custom_objects={"jaccard_coef_loss": jaccard_coef_loss, "jaccard_coef": jaccard_coef}
)

max_sizes = []
max_sizes.append(model.input_shape[1])
max_sizes.append(model.input_shape[2])

if jpype.isJVMStarted():
    print("JVM already started")
else:
    jpype.startJVM(jpype.getDefaultJVMPath(), classpath=['omero-5.6.0-jar-with-dependencies.jar'], convertStrings=True)

print(max_sizes)

for filename in tqdm(original_filenames):
    image = io.imread(filename)
    print(filename)
    #     X = []
    #     y = []

    #     original_images = []
    #     labelled_images = []
    #     image_names = []
    if (image.shape[0] <= max_sizes[0]) & (image.shape[1] <= max_sizes[1]):
        print("dedans")
        X = []
        y = []

        original_images = []
        labelled_images = []
        image_names = []
        for z_slice in range(image.shape[0]):
            original_images.append(img_as_float(image[z_slice, :, :]))
            image_names.append(filename + "_" + str(z_slice))
        for z_slice in range(image.shape[0]):
            labelled_images.append(img_as_float(image[z_slice, :, :]))

        for image in tqdm(original_images, desc="Images propres"):
            y.append(np.pad(image, ((0, max_sizes[0] - image.shape[0]), (0, max_sizes[1] - image.shape[1])),
                            mode="symmetric"))
            X.append(np.pad(image, ((0, max_sizes[0] - image.shape[0]), (0, max_sizes[1] - image.shape[1])),
                            mode="symmetric"))
        image_sizes = np.array([image.shape for image in original_images])
        assert np.all(np.array([image.shape for image in labelled_images]) == image_sizes)
        X = np.array(X)
        X = X.reshape(*X.shape, 1)
        y = np.array(y)
        y = y.reshape(*y.shape, 1)

        assert np.all(X.shape[1:] == (*max_sizes, 1))
        prediction_idx1 = np.arange(0, len(image_names))
        test_sizes1 = image_sizes[prediction_idx1]
        X_test1 = X
        y_test1 = X

        random_original_images1 = X_test1[prediction_idx1]
        random_labelled_images1 = y_test1[prediction_idx1]
        random_image_sizes1 = test_sizes1[prediction_idx1]

        predictions1 = model.predict(random_labelled_images1)
        rescaled_original1, rescaled_labelled1, rescaled_predictions1 = [], [], []
        n_images1 = len(image_names)
        for i in range(n_images1):
            rescaled_original1.append(
                random_original_images1[i][:random_image_sizes1[i][0], :random_image_sizes1[i][1], 0])
            rescaled_labelled1.append(
                random_labelled_images1[i][:random_image_sizes1[i][0], :random_image_sizes1[i][1], 0])
            rescaled_predictions1.append(predictions1[i][:random_image_sizes1[i][0], :random_image_sizes1[i][1], 0])
            rescaled_predictions1[i] = np.array(rescaled_predictions1[i]).astype(np.float32)
            rescaled_predictions1[i] = (255 * rescaled_predictions1[i]).astype(np.uint8)

        image_plus = []
        IJ = jpype.JPackage("ij").IJ

        j = 0
        # for i in (original_filenames):
        size_x = IJ.openImage(filename).getWidth();
        size_y = IJ.openImage(filename).getHeight();
        size_z = IJ.openImage(filename).getNSlices();

        imp = IJ.createHyperStack("tmp", size_x, size_y, 1, size_z, 1, 8);

        cal = IJ.openImage(filename).getCalibration();
        imp.setCalibration(cal);
        stack = imp.getImageStack();
        j_temp = j
        for j in range(j_temp, j_temp + size_z):
            values = rescaled_predictions1[j].flatten()
            values = jpype.JArray(jpype.JByte)(values)
            n = imp.getStackIndex(1, j - j_temp + 1, 1);
            stack.setPixels(values, n)
            j += 1

        imp.setStack(stack);
        imp.setOpenAsHyperStack(True);
        imp.setDisplayMode(IJ.COMPOSITE);

        image_plus.append(imp)

        IJIO = jpype.JPackage("ij.io")

        path = "TEST_APPLY_MODEL/PREDICTION/"
        # os.mkdir(path)

        for i in range(len(image_plus)):
            file_saver = IJIO.FileSaver(image_plus[i])
            # file_saver.saveAsTiff(
            #     "/home/tridubos/MACHINE_LEARNING/IMAGE_TEST/PREDICT/" + str(filename.split('/')[-1]).replace('.tif',
            #                                                                                                  '') + "_MLprediction.tif")
            file_saver.saveAsTiff(args.outputPath + "/" + str(filename.split('/')[-1]).replace('.tif',
                                                                                                            '') + "_MLprediction.tif")

        # else:
        #    print("Image trop grosse skipped, image name \n" + filename)

