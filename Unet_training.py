import os
import json
from time import time
from glob import glob

import numpy as np
import matplotlib

import tensorflow as tf

matplotlib.use('agg')
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
from skimage import io, transform, img_as_float
from tqdm import tqdm

from keras.models import Input, Model, load_model
from keras.layers import Conv2D, MaxPooling2D, UpSampling2D, Dropout, concatenate
from keras.optimizers import Adam
from keras import backend as K


from tensorflow import keras

from datetime import datetime

# Trouver toutes les images non-annotees
original_filenames = sorted(glob("./TRAINING_129/RAW/*.tif"))
print("J'ai trouve {} fichiers.".format(len(original_filenames)))

# Verifier que chaque image a son equivalent annote
for filename in original_filenames:
    if not os.path.exists(filename.replace("RAW", "GIFT")):
        print("{} n'a pas d'equivalent !".format(filename))

# Charger toutes les images en RAM, assumant le 2D :
# traiter chaquer epaisseur du z-stack comme images separees
original_images = []
labelled_images = []
image_names = []

# Iteration sur les fichiers
for filename in tqdm(original_filenames):

    # Lecture de l'image propre et separation des epaisseurs
    image = io.imread(filename)
    for z_slice in range(image.shape[0]):
        original_images.append(img_as_float(image[z_slice, :, :]))
        image_names.append(filename + "_" + str(z_slice))

    # L'image annotee
    image = io.imread(filename.replace("RAW", "GIFT"))
    for z_slice in range(image.shape[0]):
        labelled_images.append(img_as_float(image[z_slice, :, :]))

print("J'ai {} epaisseurs depuis nos {} fichiers.".format(len(original_images), len(original_filenames)))

# Trouvons la taille de chaque images en (x, y)
image_sizes = np.array([image.shape for image in original_images])
assert np.all(np.array([image.shape for image in labelled_images]) == image_sizes)

# Le max, et la puissance de 2 immediatement au dessus de ce max
max_sizes = image_sizes.max(axis=0)
transform_to = 2 ** np.ceil(np.log2(max_sizes)).astype(int)
print("Les dimensions max sont {}.".format(max_sizes))

# Transformation : on reflete les images jusqu'a la taille requise
X = []
y = []

for image in tqdm(original_images, desc="Images propres"):
    X.append(
        np.pad(image, ((0, transform_to[0] - image.shape[0]), (0, transform_to[1] - image.shape[1])), mode="symmetric"))

for image in tqdm(labelled_images, desc="Images annotees"):
    y.append(
        np.pad(image, ((0, transform_to[0] - image.shape[0]), (0, transform_to[1] - image.shape[1])), mode="symmetric"))

# On va transformer nos listes en np.array() et rajouter une epaisseur de 1
# ( au lieu d'une epaisseur vide; le reseau a besoin de savoir ca explicitement )
X = np.array(X)
X = X.reshape(*X.shape, 1)
y = np.array(y)
y = y.reshape(*y.shape, 1)

# Validation que toutes nos images ont la forme qu'il faut
assert np.all(X.shape[1:] == y.shape[1:] == (*transform_to, 1))


# Loss function : the Jaccard index
def jaccard_coef(y_true, y_pred):
    """
    The Jaccard index, where 1 is the perfect overlap.
    """
    y_true_flat = K.flatten(y_true)
    y_pred_flat = K.flatten(y_pred)
    intersection = K.sum(y_true_flat * y_pred_flat)
    union = K.sum(y_true_flat) + K.sum(y_pred_flat) - intersection
    return intersection / (union + 1e-6)


def jaccard_coef_loss(y_true, y_pred):
    """
    Jaccard loss -- negative of the Jaccard coefficient, as losses are minimised.
    """
    return -jaccard_coef(y_true, y_pred)


def u_net(image_size=X.shape[1:], loss="binary_crossentropy", summary=True):
    """
    Implementation de l'U-net pour ces images :
    - trois bloques de convolutions
    - convolutions 3x3, activation ReLU, initialisation He
    - on commence avec 64 kernels, et on passe a 256 au max
    - le premier bloque de convolutions n'a pas de dropout
    """

    # Input des images
    inputs = Input(image_size)

    # L'image fait 128x256.
    # Premier bloque convolution
    conv1 = Conv2D(64, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(inputs)
    conv1 = Conv2D(64, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv1)
    pool1 = MaxPooling2D((2, 2))(conv1)

    # L'image fait 64x128.
    # Deuxieme bloque convolution
    conv2 = Conv2D(128, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(pool1)
    conv2 = Conv2D(128, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv2)
    drop2 = Dropout(rate=0.5)(conv2)
    pool2 = MaxPooling2D((2, 2))(drop2)

    # L'image fait 32x64.
    # Troisieme block convolution.
    conv3 = Conv2D(256, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(pool2)
    conv3 = Conv2D(256, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv3)
    drop3 = Dropout(rate=0.5)(conv3)

    # On est au fond du U-net. Faut remonter !
    up4 = Conv2D(128, (2, 2), activation="relu", padding="same", kernel_initializer="he_normal")(
        UpSampling2D((2, 2))(drop3))
    merge4 = concatenate([drop2, up4], axis=3)
    conv4 = Conv2D(128, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(merge4)
    conv4 = Conv2D(128, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv4)

    # L'image fait 64x128.
    up5 = Conv2D(64, (2, 2), activation="relu", padding="same", kernel_initializer="he_normal")(
        UpSampling2D((2, 2))(conv4))
    merge5 = concatenate([conv1, up5], axis=3)
    conv4 = Conv2D(64, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(merge5)
    conv4 = Conv2D(64, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv4)

    # L'image fait 128x256, comme au debut.
    conv5 = Conv2D(1, (1, 1), activation="sigmoid")(conv4)

    # C'est l'architecture. Maintenant, on compile.
    model = Model(inputs, output=conv5)
    model.compile(optimizer=Adam(lr=1e-4), loss=loss, metrics=["accuracy", jaccard_coef])

    if summary:
        model.summary()

    return model


def u_net_deep(image_size=X.shape[1:], loss="binary_crossentropy", summary=True):
    """
    Implementation de l'U-net pour ces images :
    - trois bloques de convolutions
    - convolutions 3x3, activation ReLU, initialisation He
    - on commence avec 64 kernels, et on passe a 256 au max
    - le premier bloque de convolutions n'a pas de dropout
    """

    # Input des images
    inputs = Input(image_size)

    # L'image fait 128x256.
    # Premier bloque convolution
    conv1 = Conv2D(64, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(inputs)
    conv1 = Conv2D(64, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv1)
    pool1 = MaxPooling2D((2, 2))(conv1)

    # L'image fait 64x128.
    # Deuxieme bloque convolution
    conv2 = Conv2D(128, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(pool1)
    conv2 = Conv2D(128, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv2)
    drop2 = Dropout(rate=0.5)(conv2)
    pool2 = MaxPooling2D((2, 2))(drop2)

    # L'image fait 32x64.
    # Deuxieme bloque convolution
    conv3 = Conv2D(256, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(pool2)
    conv3 = Conv2D(256, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv3)
    drop3 = Dropout(rate=0.5)(conv3)
    pool3 = MaxPooling2D((2, 2))(drop3)

    # L'image fait 16x32.
    # Troisieme block convolution.
    conv4 = Conv2D(512, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(pool3)
    conv4 = Conv2D(512, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv4)
    drop4 = Dropout(rate=0.5)(conv4)

    # On est au fond du U-net. Faut remonter !
    up5 = Conv2D(256, (2, 2), activation="relu", padding="same", kernel_initializer="he_normal")(
        UpSampling2D((2, 2))(drop4))
    merge5 = concatenate([drop3, up5], axis=3)
    conv5 = Conv2D(256, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(merge5)
    conv5 = Conv2D(256, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv5)

    # L'image fait 32x64.
    up6 = Conv2D(128, (2, 2), activation="relu", padding="same", kernel_initializer="he_normal")(
        UpSampling2D((2, 2))(conv5))
    merge6 = concatenate([drop2, up6], axis=3)
    conv6 = Conv2D(128, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(merge6)
    conv6 = Conv2D(128, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv6)

    # L'image fait 64x128.
    up7 = Conv2D(64, (2, 2), activation="relu", padding="same", kernel_initializer="he_normal")(
        UpSampling2D((2, 2))(conv6))
    merge7 = concatenate([conv1, up7], axis=3)
    conv7 = Conv2D(64, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(merge7)
    conv7 = Conv2D(64, (3, 3), activation="relu", padding="same", kernel_initializer="he_normal")(conv7)

    # L'image fait 128x256, comme au debut.
    conv8 = Conv2D(1, (1, 1), activation="sigmoid")(conv7)

    # C'est l'architecture. Maintenant, on compile.
    model = Model(inputs, outputs=conv8)
    model.compile(optimizer=Adam(lr=1e-4), loss=loss, metrics=["accuracy", jaccard_coef])

    if summary:
        model.summary()

    return model


# On va creer des indexes, separant chaque image en training ou en test set
training_idx = np.random.choice(range(len(X)), int(0.9 * len(X)), replace=False)
testing_idx = [i for i in range(len(X)) if i not in training_idx]

X_train = X[training_idx]
y_train = y[training_idx]
X_test = X[testing_idx]
y_test = y[testing_idx]

train_sizes = image_sizes[training_idx]
test_sizes = image_sizes[testing_idx]

logdir = "./TEST_EARLY_STOP/logs/scalars/" + datetime.now().strftime("%Y%m%d-%H%M%S")
tensorboard_callback = keras.callbacks.TensorBoard(log_dir=logdir)
tensorboard_callback = tf.keras.callbacks.EarlyStopping(monitor='loss', patience=5)

tic = time()
model = u_net_deep(loss=jaccard_coef_loss)
# Entrainement du u-net, validant sur le test set
history = model.fit(
    X_train, y_train,
    batch_size=20,
    epochs=50,
    validation_data=(X_test, y_test),
    verbose=2,
    callbacks=[tensorboard_callback]
)

print("Entrainement en {:.1f} minutes.".format((time() - tic) / 60))
all_predictions = model.predict(X_test)
thresholds = np.linspace(0, 1 - 1e-8, 201)
model.save("./TEST_EARLY_STOP/unet_profond_COL0_129_OLD_20E_EARLY_STOP_TEST.h5")

# model.save("unet_profond_COL0_129_OLD.h5")




